import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { EmpleadoComponent } from './components/empleados/empleado/empleado.component';
import { ListaEmpleadoComponent } from './components/lista_empleado/lista_empleado.component';
import { ToastrModule } from 'ngx-toastr';




@NgModule({
  declarations: [
    AppComponent,
    ListaEmpleadoComponent,
    EmpleadoComponent,
    EmpleadosComponent
    ],
  imports: [
    BrowserModule,
   FormsModule,
   HttpModule,
   ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
