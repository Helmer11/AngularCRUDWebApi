
import { Empleado } from './empleado.model';
import {  Headers,  Http,  RequestMethod,  RequestOptions,  Response  } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { ListaEmpleadoComponent } from '../lista_empleado/lista_empleado.component';



@Injectable()

export class EmpleadoService {

 listaEmpleado: Empleado[];
 public empleadoL: Empleado = {
    empleadoID: 0,
    Nombre: '',
   Apellido: '',
   CodigoEmp: '',
   Posicion: '',
   Cuidad: ''
  };

  constructor(private _http: Http) {

  }

  public postEmpleado(emp: Empleado) {

    const body = JSON.stringify(emp);
    const headerOptions = new Headers({ 'Content-type': 'application/json' });
    const requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
    return  this._http.post('http://localhost:52287/api/Empleado/', body, requestOptions)
             .map(x => {
                   x.json();
                 });
  }

  public putEmpleado(id, emp) {

    const body = JSON.stringify(emp);
    const headerOptions = new Headers({ 'Content-type': 'application/json' });
    const requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return  this._http.put('http://localhost:52287/api/Empleado/' + id, body, requestOptions)
             .map(x => {
                   x.json();
                 });
  }

  getListaEmpleado() {
    this._http.get('http://localhost:52287/api/Empleado/')
    .map((data: Response) => {
      return data.json() as Empleado[] ;
    }).toPromise().then(x => {
      this.listaEmpleado = x;
    });
  }

  deleteEmpleado(id: number) {
    return this._http.delete('http://localhost:52287/api/Empleado/' + id).map((res) =>  {
      res.json();
    });
  }

}
