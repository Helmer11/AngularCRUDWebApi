import { JSONP_ERR_WRONG_METHOD } from '@angular/common/http/src/jsonp';
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from '../shared/empleado.service';
import { Empleado } from '../shared/empleado.model';
import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;


@Component({
  selector: 'app-lista-empleado',
  templateUrl: './lista_empleado.component.html',
  styles: []
})
export class ListaEmpleadoComponent implements OnInit {

  constructor(private empleadoService: EmpleadoService) { }

  ngOnInit() {
    this.empleadoService.getListaEmpleado();
  }
    public showForEdit(emp: Empleado) {
      this.empleadoService.empleadoL = Object.assign({}, emp);
    }

    onDelete(id: number ) {

      swal({title: 'Esta seguro que desea eliminar el registro',
      text: 'Una vez eliminado, no podrá recuperar el registro',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
        })
      .then((willDelete) => {
        if (willDelete) {
          this.empleadoService.deleteEmpleado(id)
          .subscribe(x => {
            this.empleadoService.getListaEmpleado();
          });
          swal('Su Registro fue eliminado', {
            icon: 'success',
          });
        } else {
         // swal('Your imaginary file is safe!');
        }
      });

    }

}
