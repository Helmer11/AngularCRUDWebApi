import { Empleado } from './../../shared/empleado.model';

import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;

 import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from '../../shared/empleado.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']
})
export class EmpleadoComponent implements OnInit {

  constructor(public empleadoService: EmpleadoService,
              private _toatr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  public resetForm(form?: NgForm) {

    if (form != null) {
       form.reset();
      this.empleadoService.empleadoL = {
        empleadoID: 0,
      Nombre: '',
      Apellido: '',
      CodigoEmp: '',
      Posicion: '',
      Cuidad: ''
    };
    }
  }

  public onSubmit( form: NgForm ) {
      console.log(form.value);
      // tslint:disable-next-line:no-debugger
        debugger;
          if (form.value.empleadoID === 0 ) {
        this.empleadoService.postEmpleado(form.value)
        .subscribe( data => {
          this.resetForm(form);
          this.empleadoService.getListaEmpleado();
          // this._toatr.success('El ingreso fue satisfactoria', ' Empleado Registrado');
          swal('Registro Realizado! ', 'El ingreso fue satisfactoria!', 'success');

        });
  } else {
    this.empleadoService.putEmpleado(form.value.EmpleadoID, form.value)
    .subscribe( data => {
      this.resetForm(form);
      this.empleadoService.getListaEmpleado();
      // this._toatr.success('El ingreso fue satisfactoria', ' Empleado Registrado');
      swal('Registro Realizado! ', 'El ingreso fue satisfactoria!', 'success');

    });
  }
}
}
